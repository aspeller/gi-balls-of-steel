﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject stopwatchPrefab = null;

    private bool timerON = false;

    private float timer = 0.0f;

    // static singleton instance
    private static GameManager _Instance = null;

    public static GameManager Instance
    {
        get
        {
            // if we dont have an instance of the gameManger then go find it in the hierarchy
            if (_Instance == null)
            {
                _Instance = (GameManager)FindObjectOfType(typeof(GameManager));
            }
            // return the instance
            return _Instance;
        }
    }

    // Use this for initialization
    void Start()
    {


        StartCoroutine(CreateObjects(2.0f));
    }

    private IEnumerator CreateObjects(float objectDelay)
    {
        float time = 0.0f;

        // wait 4 seconds before begining to make new objects
        while (time < 4.0f)
        {
            time += Time.deltaTime;
            yield return null;  // come back during the next frame
        }

        for (int i = 0; i < 50; i++)
        {
            time = 0;
            // delay for 1 second between the creation of each new object
            while (time<0.3f)
            {
                time += Time.deltaTime;
                yield return null;
            }
            Instantiate(stopwatchPrefab, transform.position + (Random.insideUnitSphere * 100), Quaternion.identity);
        }

        time = 0;
        while (time < 10.0f)
        {
            time += Time.deltaTime;
            yield return null;
        }

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0)) { timerON = !timerON; }
        if (Input.GetMouseButtonDown(1)) { timer = 0; }
        if (timerON) { timer += Time.deltaTime; }

    }

    public float GetTimer()
    {
        return timer;
    }
}
