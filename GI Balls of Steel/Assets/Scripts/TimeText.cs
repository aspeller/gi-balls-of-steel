﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeText : MonoBehaviour {

    //public GameManager myManager = null;
    private TextMesh textMesh = null;

	// Use this for initialization
	void Start () {
        textMesh = GetComponent<TextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
        textMesh.text = GameManager.Instance.GetTimer().ToString("0.#") + " seconds";
	}
}
