﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotator : MonoBehaviour {

    public float xRotation = 0.0f;
    public float yRotation = 0.0f;
    public float zRotation = 0.0f;

    private Transform cogTransform;

	// Use this for initialization
	void Start () {
        cogTransform = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        cogTransform.Rotate(Time.deltaTime * xRotation, Time.deltaTime * yRotation, Time.deltaTime * zRotation);
	}
}
